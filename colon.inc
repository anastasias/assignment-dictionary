%define elem 0

%macro colon 2
	%ifstr %1
		%ifid %2
			%2: dq elem
		%else
			dq 0
		%endif
		%define elem %2
		db %1, 0
	%endif
%endmacro
