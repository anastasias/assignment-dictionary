global find_word

extern string_equals

section .text

find_word:
.circle:
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    cmp rax,0
    jne .end
    mov rsi, [rsi]
    cmp rsi, 0
    jne .circle
    mov rax, 0
    ret
.end:
    mov rax, rsi
    ret
