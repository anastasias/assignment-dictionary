%include "lib.inc"
%include "colon.inc"
%include "words.inc"

extern find_word

global _start

section .data

key_not_found: db "Key not found", 0
message_long: db "Message is long", 0
word_buffer: times 256 db 0



section .text

_start:

    mov rdi, word_buffer	
    mov rsi, 256
    call read_word
    cmp rax, 0
    je .error	
    mov rsi, elem	
    mov rdi, word_buffer
    call find_word
    cmp rax, 0
    jne .success	
    mov rdi, key_not_found
    call print_error
    jmp .end


.error:
    mov rdi, message_long
    call print_error
    jmp .end



.success:
    mov rdi, rax	
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    inc rdi
    add rdi, rax
    call print_string


.end:
    call exit
