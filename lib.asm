global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global string_copy
global print_error





section .text 
; Принимает код возврата и завершает текущий процесс


print_error:
		push rdi			
		mov rsi, rdi 		
		call string_length	
		pop rsi			
		mov rdx, rax		
		mov rax, 1			
		mov rdi, 2			
		syscall
		ret

exit: 
    mov rax,60 ; Запись номера системного вызов
    syscall ; системный вызов 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
	cmp byte[rdi+rax],0 ; проверка, является ли текущий символ строки 0
	je .end
	inc rax
	jmp .loop
    .end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rax, 1
    mov rdi, 1
    mov rsi, 0xA
    mov rdx, 1
    syscall 
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
        xor rax, rax
	mov r9, 10
	mov rax, rdi
	mov r10, rsp
	dec rsp
	.loop:
		xor rdx, rdx
		div r9
		mov rdi,rdx
		add rdi, '0'
		dec rsp
		mov byte [rsp], dil
		cmp rax, 0
		je .print
		jmp .loop
	.print:
		mov rdi, rsp
		call print_string
		mov rsp, r10
		ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jns .continue
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .continue:
	jmp print_uint
    ret 
      


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
    .loop:
	mov r8b, [rdi + rcx]
	mov r9b, [rsi + rcx]
        cmp r8b, r9b
        jne .notequal
	cmp r8b, 0
        je .equal
        inc rdi
	inc rsi

	jmp .loop
    .notequal:
	mov rax, 0 
    	ret
    .equal:
	mov rax, 1 
	ret
	

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    mov rsi, rsp
    mov rdi, 0
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    .begin:		 ;пропускаем в начале слова, табуляцию, пробел и перевод строки
   	push rdi 
   	push rsi
	push rdx
	call read_char
	pop rdx
        pop rsi
        pop rdi 
	cmp rsi,0
	je .fail 
	cmp rax, 0x20
	je .begin
	cmp rax, 0x9
	je .begin
	cmp rax, 0xA
	je .begin
	mov rdx, 0 
    .loop: 
	cmp rax,0
	je .end
	cmp rax, 0x9
	je .end
	cmp rax, 0xA
	je .end
	cmp rax, 0x20
	je .end
	mov byte[rdi+rdx], al ; запись символа в ячейку буфера
	inc rdx
	cmp rdx, rsi
	je .fail 
	push rdi
	push rsi
	push rdx
	call read_char
	pop rdx
	pop rsi
	pop rdi
	jmp .loop
    
    .fail:
	xor rdx, rdx
	mov rax,0
	ret
    .end:
	mov byte[rdi+rdx], 0
	mov rax, rdi
	ret
   
	   	
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    .loop: 
	mov r8b, byte[rdi+rdx]
	cmp r8b, 0
 	je .end
	cmp r8b, '0'
	jl .end
	cmp r8b, '9'
	jg .end
	sub r8b, '0'
	push rdx
	mov r9, 10
	mul r9
	pop rdx
	add rax, r8
	inc rdx
	jmp .loop
    .end:	
    	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'
    je .neg
    call parse_uint
    ret
    .neg: 
	inc rdi
        call parse_uint	
	inc rdx
	neg rax
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8
    push rdi
    call string_length
    pop rdi
    cmp rax, rdx
    jl .loop
    mov rax, 0
    ret
    .loop:
	mov r8b, [rdi+rcx]
	mov [rsi+rcx], r8b
	inc rcx
	cmp byte [rsi + rcx], 0
	jne .loop
	mov rax, rdx
	ret
