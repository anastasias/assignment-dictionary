lib.o: lib.asm
	nasm -felf64 -g   -o $@ $<

dict.o: dict.asm lib.inc
	nasm -felf64 -g  -o $@ $<

main.o:	main.asm lib.inc colon.inc words.inc
	nasm -felf64 -g  -o $@ $<

test: main.o lib.o dict.o
	ld -o $@ $< lib.o dict.o

